/*
 * ring_buffer.h
 *
 *  Created on: Nov 7, 2020
 *      Author: Przemo
 */

#ifndef INC_RING_BUFFER_H_
#define INC_RING_BUFFER_H_

#define RING_BUFFER_SIZE 32

typedef enum
{
	RB_OK = 0,
	RB_ERROR = 1
}RB_Status;

typedef struct
{
	uint8_t Head;
	uint8_t Tail;
	uint8_t Buffer[RING_BUFFER_SIZE];
}RingBuffer;

RB_Status RB_Read(RingBuffer *rb, uint8_t *Value);
RB_Status RB_Write(RingBuffer *rb, uint8_t Value);


#endif /* INC_RING_BUFFER_H_ */
