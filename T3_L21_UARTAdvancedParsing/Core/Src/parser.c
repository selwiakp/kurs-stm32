/*
 * parser.c
 *
 *  Created on: 11 lis 2020
 *      Author: Przemo
 */
#include "main.h"
#include "string.h"
#include "parser.h"
#include "uartdma.h"
#include "stdlib.h"
#include "stdio.h"

extern UARTDMA_HandleTypeDef huartdma2;
char Message[64];
char MyName[32] = {"No Name"};


void UART_ParseLED()
{
	  uint8_t LedState;
	  char* ParsePointer = strtok(NULL, ",");

	  if(strlen(ParsePointer) > 0)
	  {
		  if(ParsePointer[0] < '0' || ParsePointer[0] > '9')
		  {
			  UARTDMA_Print(&huartdma2, "LED wrong value. Don't use letters mate!\r\n");
			  return;
		  }
		  LedState = atoi(ParsePointer);

		  if(LedState == 1)
		  {
			  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
			  UARTDMA_Print(&huartdma2, "LED ON!\r\n");
		  }
		  else if(LedState == 0)
		  {
			  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
			  UARTDMA_Print(&huartdma2, "LED OFF!\r\n");
		  }
		  else
		  {
			  UARTDMA_Print(&huartdma2, "LED wrong number, use 1 or 0!\r\n");
		  }
	  }
}

void UART_ParseENV()
{
	  uint8_t i,j;
	  float EnvParameters[3];

	  for( i = 0; i < 3; i++)
	  {
		  char* ParsePointer = strtok(NULL, ",");
		  if(strlen(ParsePointer) > 0)
		  	  {
		  	  	  for(j=0; ParsePointer[j] != 0; j++)
		  	  	  {
		  	  		  if((ParsePointer[j] < '0' || ParsePointer[j] > '9') && ParsePointer[j] != '.')
		  	  		  {
		  	  			  sprintf(Message, "ENV wrong value. Don't use letter dude!\r\n");
		  	  			  UARTDMA_Print(&huartdma2, Message);
		  	  			  return;
		  	  		  }
		  	  		EnvParameters[i] = atof(ParsePointer);
		  	  	  }
		  	  }
		  else
		  {
			  sprintf(Message, "Too less values. ENV=X,Y,Z\r\n");
			  UARTDMA_Print(&huartdma2, Message);
			  return;
		  }
	  }
	  sprintf(Message, "Temperature: %.1f\r\n", EnvParameters[0]);
	  UARTDMA_Print(&huartdma2, Message);

	  sprintf(Message, "Humidity: %.1f\r\n", EnvParameters[1]);
	  UARTDMA_Print(&huartdma2, Message);

	  sprintf(Message, "Pressure: %.1f\r\n", EnvParameters[2]);
	  UARTDMA_Print(&huartdma2, Message);

}

void UART_ParseNAME()
{
	char* ParsePointer = strtok(NULL, ",");

	 if(strlen(ParsePointer) > 0)
	 {
		 if(strcmp(ParsePointer, "?") == 0)
		 {
		  sprintf(Message, "My name is %s \r\n", MyName);
		 }
		 else
		 {
			 strcpy(MyName, ParsePointer);
			 sprintf(Message, "Name changed to %s \r\n", MyName);
		 }

	 }
	 else
	 {
		  sprintf(Message, "Name cannot be empty!\r\n");
	 }
	  UARTDMA_Print(&huartdma2, Message);

}

void UART_ParseLine(UARTDMA_HandleTypeDef *huartdma)
{
		  char BufferReceive[64];

		  if(!UARTDMA_GetLineFromReceiveBuffer(huartdma, (char*)BufferReceive))
		  {

			  char* ParsePointer = strtok(BufferReceive, "=");

			  if(strcmp(ParsePointer, "LED") == 0)
			  {
				  UART_ParseLED();
			  }
			  else if(strcmp(ParsePointer, "ENV") == 0)
			  {
			  		UART_ParseENV();
			  }
			  else if(strcmp(ParsePointer, "NAME") == 0)
			  {
			  		UART_ParseNAME();
			  }
		   }
}
