/*
 * ring_buffer.c
 *
 *  Created on: Nov 7, 2020
 *      Author: Przemo
 */
#include "main.h"
#include "ring_buffer.h"
RB_Status RB_Read(RingBuffer *rb, uint8_t *Value)
{
	if(rb->Head == rb->Tail)
	{
		return RB_ERROR;
	}

	*Value = rb->Buffer[rb->Tail];

	rb->Tail = (rb->Tail + 1) % RING_BUFFER_SIZE;
	return RB_OK;
}

RB_Status RB_Write(RingBuffer *rb, uint8_t Value)
{
	uint8_t TmpHead = (rb ->Head + 1) % RING_BUFFER_SIZE;

	if(TmpHead == rb->Tail)
	{
		return RB_ERROR;
	}

	rb->Buffer[rb->Head] = Value;
	rb->Head = TmpHead;
	return RB_OK;
}
