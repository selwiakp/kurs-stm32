/*
 * parser.h
 *
 *  Created on: 11 lis 2020
 *      Author: Przemo
 */

#ifndef INC_PARSER_H_
#define INC_PARSER_H_
#include "uartdma.h"

void UART_ParseLine(UARTDMA_HandleTypeDef *huartdma);

#endif /* INC_PARSER_H_ */
