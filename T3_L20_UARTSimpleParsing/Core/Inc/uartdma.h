/* INC_UARTDMA_H_ */

/*
 * uartdma.h
 *
 *  Created on: Nov 8, 2020
 *      Author: Przemo
*/

#ifndef INC_UARTDMA_H_
#define INC_UARTDMA_H_

#include "ring_buffer.h"

#define  DMA_RX_BUFFER_SIZE 64
#define  DMA_TX_BUFFER__SIZE 64


typedef struct
{
	UART_HandleTypeDef* huart;

	uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];
	RingBuffer UART_RX_Buffer;
	uint8_t UartRxBufferLines;

	uint8_t DMA_TX_Buffer[DMA_TX_BUFFER__SIZE];
	RingBuffer UART_TX_Buffer;
	uint8_t UartTxBufferLines;

}UARTDMA_HandleTypeDef;

void UARTDMA_Init(UARTDMA_HandleTypeDef *huartdma, UART_HandleTypeDef *huart);

void UARTDMA_UartIrqHandler(UARTDMA_HandleTypeDef *huartdma);

void UARTDMA_DmaReceiveIrqHandler(UARTDMA_HandleTypeDef *huartdma);

void UARTDMA_Print(UARTDMA_HandleTypeDef *huartdma, char *Message);

void UARTDMA_TransmitEvent(UARTDMA_HandleTypeDef *huartdma);

uint8_t UARTDMA_GetLineFromReceiveBuffer(UARTDMA_HandleTypeDef *huartdma, char *OutBuffer);

uint8_t UARTDMA_IsDataReceivedReady(UARTDMA_HandleTypeDef *huartdma);


#endif /* INC_UARTDMA_H_ */
